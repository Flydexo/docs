# Assets
## Logotype
### Logo
- Transparent (svg)
![](https://bezier.fi/favicon.svg)
[](https://bezier.fi/favicon.svg)
## Colors
View design system on Figma [here](#figma)
## Figma (Design System and App)
[](https://www.figma.com/file/AjP4s160ihO1XMj6swIGCK/WebApp?node-id=0%3A1&t=lDx40FQJpWFQcGXE-1)
